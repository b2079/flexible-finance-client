export const environment = {
  production: true,
  apiUrl: 'https://flexible-finance-server.herokuapp.com',
};
